const jwt = require('jsonwebtoken');
const { extractToken } = require('../utils/Helpers');
const fs = require('fs');
const privateKey = fs.readFileSync('resources/private.key');
const Response = require('../utils/Response');

let authenticateToken = (req, res, next) => {
  
    const token = extractToken(req.headers);
    if (token == null) return res.status(401).send(new Response('error', 'Access token expired', null, 401, null));
    let data = null;
    try {        
        data = jwt.verify(token, privateKey);
    } catch(err) {
        return res.status(401).send(new Response('error', 'Access token expired', null, 401, null));
    }
    req.user = data.username;
    next();

}

module.exports = { authenticateToken }