const models = require('../db/models');
const { Op } = require("sequelize");
const ErrorApp = require('../utils/Error');

let checkPerm = (username, resource) => {
    let perms = Array.isArray(resource) ? resource : [resource];
    let user = await models.User.findOne({where:{[Op.or]:[{username}, {email:username}]}});
    if(user == null) throw new ErrorApp('User does not exists', 404);
    let userRol = await models.Rol.findOne({where:{id:user.RolId}, include:{model:models.Permission, as:'permissions'}});
    if(userRol == null) throw new ErrorApp('The user does not have rol', 400);
    for(let perm of userRol.permissions){
        if(perms.includes(perm.name)) return true;
    }
    return false;
}

module.exports = { checkPerm }