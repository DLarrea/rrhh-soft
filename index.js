require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require("http");
const app = express();
const port = process.env.APP_PORT;
const server = http.createServer(app);
const routes = require('./routes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use('/api/v1', routes);

server.listen(port, () => {
    console.log(`Server running on port ${port}`);
});