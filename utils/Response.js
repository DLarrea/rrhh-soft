class Response {
    constructor(status, message, data, code, meta){
        this.status = status;
        this.message = message;
        this.data = data;
        this.code = code != null? code: 200;
        this.meta = meta != null? meta: null;
    }
}

module.exports = Response;