const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const configurationService = require('../services/configurationService');
const { validationResult } = require('express-validator');
const { checkPerm } = require('../middlewares/checkPerm');
const { functionMapper } = require('../db/data/permissions');

let viewConfiguration = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['viewConfiguration'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        let configurations = await configurationService.view();
        return res.status(200).send(new Response('success', null, configurations, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let createConfiguration = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['createConfiguration'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        await configurationService.create(req.body);
        return res.status(200).send(new Response('success', 'Registry was created successfully', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let updateConfiguration = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['updateConfiguration'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        await configurationService.update(req.params.id, req.body);
        return res.status(200).send(new Response('success', 'Registry was created successfully', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let deleteConfiguration = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['deleteConfiguration'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        await configurationService.destroy(req.params.id);
        return res.status(200).send(new Response('success', 'Registry was delete successfully', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let deleteConfigurationLdap = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['deleteConfiguration'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        await configurationService.destroyLdapConfiguration();
        return res.status(200).send(new Response('success', 'Registry was delete successfully', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

module.exports = {
    viewConfiguration,
    createConfiguration,
    updateConfiguration,
    deleteConfiguration,
    deleteConfigurationLdap
}