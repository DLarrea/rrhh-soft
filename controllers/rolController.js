const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const rolService = require('../services/rolService');
const { validationResult } = require('express-validator');
const { checkPerm } = require('../middlewares/checkPerm');
const { functionMapper } = require('../db/data/permissions');

let viewRol = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['viewRol'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        let rols = await rolService.view();
        return res.status(200).send(new Response('success', null, rols, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let createRol = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['createRol'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        await rolService.create(req.body);
        return res.status(200).send(new Response('success', 'Registry was created successfully', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let updateRol = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['updateRol'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        await rolService.update(req.params.id, req.body);
        return res.status(200).send(new Response('success', 'Registry was created successfully', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let deleteRol = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!checkPerm(req.user, functionMapper['deleteRol'])) throw new ErrorApp('The user does not have permission to this resource', 403);
        await configurationService.destroy(req.params.id);
        return res.status(200).send(new Response('success', 'Registry was delete successfully', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let createRolPermission = async (req, res) => {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        
        await rolService.addPermission(req.body);
        return res.status(200).send(new Response('success', 'Permission associated', null, 200, null));
        
    } catch(e){

        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    
    }
}

let deleteRolPermission = async (req, res) => {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        
        await rolService.addPermission(req.body);
        return res.status(200).send(new Response('success', 'Permission associated', null, 200, null));
        
    } catch(e){

        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    
    }
}

module.exports = {
    viewRol,
    createRol,
    updateRol,
    deleteRol,
    createRolPermission,
    deleteRolPermission
}