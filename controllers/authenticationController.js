const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const authenticationService = require('../services/authenticationService');
const { validationResult } = require('express-validator');

let login = async (req, res) => {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));

        let result = await authenticationService.login(req.body);
        return res.status(200).send(new Response('success', 'Authentication successfully', result, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    
    }
}

let refreshToken = async (req, res) => {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));

        let result = await authenticationService.refreshToken(req.headers);
        return res.status(200).send(new Response('success', 'Authentication successfully', result, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    
    }
}

module.exports = {
    login,
    refreshToken
}