const express = require('express');
const router = express.Router();
const { body } = require('express-validator');
const configurationController = require('../controllers/configurationController');
router.get(
    '/configuration', 
    configurationController.viewConfiguration
);
router.post(
    '/configuration', 
    body('attribute').not().isEmpty().trim().withMessage('attribute required'),
    body('value').not().isEmpty().trim().withMessage('value required'),
    configurationController.createConfiguration
);
router.put(
    '/configuration/:id', 
    body('attribute').not().isEmpty().trim().withMessage('attribute required'),
    body('value').not().isEmpty().trim().withMessage('value required'),
    configurationController.updateConfiguration
);
router.delete(
    '/configuration/:id', 
    configurationController.deleteConfiguration
);
router.delete(
    '/configuration', 
    configurationController.deleteConfigurationLdap
);

module.exports = router;