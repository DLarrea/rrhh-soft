const app = require('express').Router();
const { authenticateToken } = require('../middlewares/authenticateToken');

app.use('/admin', authenticateToken, require('./rolRoute'));
app.use('/admin', authenticateToken, require('./configurationRoute'));
app.use('/authentication', require('./authenticationRoute'));

module.exports = app