const express = require('express');
const router = express.Router();
const { body, header } = require('express-validator');
const authenticationController = require('../controllers/authenticationController');
router.post(
    '/login', 
    body('username').not().isEmpty().trim().withMessage('username required'),
    body('password').not().isEmpty().trim().withMessage('password required'),
    authenticationController.login
);
router.post(
    '/refresh-token', 
    header('authorization').not().isEmpty().withMessage('authorization header required'),
    authenticationController.login
);

module.exports = router;