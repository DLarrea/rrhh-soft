const express = require('express');
const router = express.Router();
const { body, param } = require('express-validator');
const rolController = require('../controllers/rolController');

router.get('/rol', rolController.getAllRols);
router.post(
    '/rol', 
    body('name').not().isEmpty().trim().withMessage('name required'),
    rolController.addRol
);
router.put(
    '/rol/:id', 
    body('name').not().isEmpty().trim().withMessage('name required'),
    param('id').isNumeric().withMessage('rol id required'),
    rolController.updateRol
);
router.delete(
    '/rol/:id', 
    param('id').isNumeric().withMessage('rol id required'),
    rolController.removeRol
);
router.post(
    '/rol/permission', 
    body('RolId').isNumeric().withMessage('rol id required'),
    body('PermissionId').isNumeric().withMessage('permission id required'),
    rolController.addRolPermission
);

module.exports = router;