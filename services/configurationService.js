const models = require('../db/models');
const ErrorApp = require('../utils/Error');

let view = async() => {
    return await models.Configuration.findAll();
} 

let create = async({attribute, value}) => {
    switch(attribute){
        case 'LDAP_URL':
            if(!/^(ldap|http|https):\/\/[^ "]+$/.test(value)) throw new ErrorApp('Invalid LDAP_URL value', 400);
            break;
        case 'LDAP_TLS':
            if(!['S','N'].includes(value)) throw new ErrorApp('Invalid LDAP_TLS value', 400);
            break;
        default :
            break;
    }
    return await models.Configuration.create({value, attribute});
}

let update = async(id, {attribute, value}) => {
    switch(attribute){
        case 'LDAP_URL':
            if(!/^(ldap|http|https):\/\/[^ "]+$/.test(value)) throw new ErrorApp('Invalid LDAP_URL value', 400);
            break;
        case 'LDAP_TLS':
            if(!['S','N'].includes(value)) throw new ErrorApp('Invalid LDAP_TLS value', 400);
            break;
        default :
            break;
    }
    return await models.Configuration.update({value, attribute}, {where:{id}});
}

let destroyLdapConfiguration = async() => {
    await models.Configuration.destroy({where:{attribute:{
        [Op.like]: 'LDAP%'
    }}});
    return await models.Configuration.create({value: 'N', attribute: 'LDAP'});
}

let destroy = (id) => {
    return await models.Configuration.destroy({where:{id}});
}

module.exports = {
    view,
    create,
    update,
    destroyLdapConfiguration,
    destroy
}