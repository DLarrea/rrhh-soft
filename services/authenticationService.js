const models = require('../db/models');
const { Op } = require("sequelize");
const fs = require('fs');
const ErrorApp = require('../utils/Error');
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);
const privateKey = fs.readFileSync('resources/private.key');
const jwt = require('jsonwebtoken');
const ldap = require('ldapjs');
const { signOptionsToken, signOptionsTokenRefresh } = require('../resources/tokenSignOptins');
const { extractToken } = require('../utils/Helpers');

let login = async({username, password}) => {
    
    
    let ldap = await getLdapConfiguration();
    let user = await models.User.findOne({attributes: ['id','username', 'password', 'authType', 'email', 'RolId', 'active'], where:{[Op.or]:[{username}, {email:username}]}});
    
    if(user != null && ldap == null && user.authType == 'APP'){
        if(!user.active) throw new ErrorApp('User is not active', 400);
        if(!bcrypt.compareSync(password, user.password)) throw new ErrorApp('Username or password incorrect', 400);
        await models.User.update({lastLogon: new Date()}, {where:{id:user.id}});
    }
    
    
    if(user != null && ldap != null && user.authType == 'LDAP'){
        if(!user.active) throw new ErrorApp('User is not active', 400);
        let ldapUser = await ldapAuthentication(username, password, ldap);
        if(ldapUser == null) throw new ErrorApp('Username or password incorrect', 400);
        let encryptedPw = bcrypt.hashSync(password, salt);
        if(!bcrypt.compareSync(password, user.password)) await models.User.update({password: encryptedPw}, {where:{id:user.id}});
        else await models.User.update({lastLogon: new Date()}, {where:{id:user.id}});
    }


    if(user == null && ldap == null){
        throw new ErrorApp('Username does not exists', 404);
    }

    if(user == null && ldap != null){
        let ldapUser = await ldapAuthentication(username, password, ldap);
        if(ldapUser == null) throw new ErrorApp('Username or password incorrect', 400);
        let encryptedPw = bcrypt.hashSync(password, salt);
        let rol = await models.Rol.findOne({where:{name:'VISITOR'}});
        let {dataValues} = await models.User.create({username:ldapUser.samaccountname, password: encryptedPw, authType:'LDAP', RolId: rol.id, email: ldapUser.mail || null, lastLogon: new Date() });
        user = dataValues;
    }

    signOptionsToken.subject = username;
    signOptionsTokenRefresh.subject = username;
    let userRol = await models.Rol.findOne({where:{id:user.RolId}, include:{model:models.Permission, as:'permissions'}});

    return {
        access_token: jwt.sign({...user, rol: userRol}, privateKey, signOptionsToken),
        refresh_token: jwt.sign({...user, rol: userRol}, privateKey, signOptionsTokenRefresh)
    }
    
}

let refreshToken = async(headers) => {
    
    let data = null;
    let refreshToken = extractToken(headers);
    if(refreshToken == null) throw new ErrorApp('Refresh token expired', 401);
    try {        
        data = jwt.verify(refreshToken, privateKey);
    } catch(err) {
        throw new ErrorApp('Refresh token expired', 401);
    }

    signOptionsToken.subject = data.username;
    signOptionsTokenRefresh.subject = data.username;

    return {
        access_token: jwt.sign({...user, rol: userRol}, privateKey, signOptionsToken),
        refresh_token: jwt.sign({...user, rol: userRol}, privateKey, signOptionsTokenRefresh)
    }

}

let ldapAuthentication = async(username, password, ldapProperties) => {

    let client = ldap.createClient({
        url: ldapProperties.LDAP_URL
    });    

    client.on('error', error => {
        return null;
    });

    let user = await new Promise((resolve) => {
        try {
            client.bind(
                `${ldapProperties.LDAP_USER_ADMIN}@${ldapProperties.LDAP_DOMAIN}`, 
                ldapProperties.LDAP_USER_ADMIN_PW, 
                async (err) => {
                    if(err) {
                        client.destroy();
                        resolve(null)
                    };
                    client.search(
                        ldapProperties.LDAP_SEARCH_OU,
                        {
                            filter: `(&(objectClass=user)(|(samaccountname=${username})(mail=${username})))`,
                            attributes: ['samaccountname', 'mail'],
                            scope: 'sub'
                        },
                        (error, search) => {
                            if(error) {
                                client.destroy();
                                resolve(null);
                            }
                            search.on('searchEntry', async (entry) => {
                                resolve(entry.object);
                            });
                            search.on('error', (err) => {
                                client.destroy();
                                resolve(null);
                            });
                        }
                    );
                }
            );
        } catch(e){
            client.destroy();
            resolve(null);
        }
    }); 

    if(user == null || !user.hasOwnProperty('dn')) return null;

    return await new Promise((resolve) => {
        try {
            client.bind(
                user.dn, 
                password, 
                async (err) => {
                    if(err) {
                        client.destroy();
                        resolve(null)
                    };
                    resolve({samaccountname: user.sAMAccountName || user.samaccountname, mail:user.mail});
                }
            );
        } catch(e){
            client.destroy();
            resolve(null);
        }
    });  
}

let getLdapConfiguration = async() => {
    let ldap = new Object();
    let ldapConfiguration = await models.Configuration.findAll({where:{attribute:{
        [Op.like]: 'LDAP%'
    }}});
    if(ldapConfiguration == null) return null;
    for(let conf of ldapConfiguration) ldap[conf.attribute] = conf.value;
    //Required attributes
    if( !ldap.hasOwnProperty('LDAP')
        || ldap.LDAP != 'S'
        || !ldap.hasOwnProperty('LDAP_URL') 
        || !ldap.hasOwnProperty('LDAP_SEARCH_OU')
        || !ldap.hasOwnProperty('LDAP_USER_ADMIN')
        || !ldap.hasOwnProperty('LDAP_USER_ADMIN_PW')
        || !ldap.hasOwnProperty('LDAP_DOMAIN')
    ) return null;
    return ldap;
}

module.exports = {
    login,
    refreshToken
}
