const models = require('../db/models');
const ErrorApp = require('../utils/Error');

let view = async() => {
    return await models.Rol.findAll();
} 

let create = async({name}) => {
    return await models.Rol.create({name});
}

let update = async(id, {name}) => {
    return await models.Rol.update({name}, {where:{id}});
}

let destroy = (id) => {
    return await models.Rol.destroy({where:{id}});
}

let createRolPermission = async({rolId:RolId, permissionId:PermissionId}) => {
    let count = await models.RolPermission.count({where:{RolId, PermissionId}});
    if(count > 0) throw new ErrorApp(`Permission ${PermissionId} is already associated whit Rol ${RolId}`)
    return await models.RolPermission.create({value, attribute});
}

let destroyRolPermission = (id) => {
    return await models.RolPermission.destroy({where:{id}});
}

module.exports = {
    view,
    create,
    update,
    destroy,
    createRolPermission,
    destroyRolPermission
}