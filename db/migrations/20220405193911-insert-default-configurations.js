'use strict';
const { rols, adminRols } = require('../data/defaultConfigurations');

module.exports = {
  async up (queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert('Rol', rols);
      await queryInterface.bulkInsert('RolPermission', adminRols());
    } catch(e){
      throw new Error('Error inserting rols and permissions');
    }
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
