'use strict';
const { permissions } = require('../data/permissions');

module.exports = {
  async up (queryInterface, Sequelize) {
    try {
      queryInterface.bulkInsert('Permission', permissions);
    } catch(e){
      throw new Error('Error inserting permissions');
    }
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
