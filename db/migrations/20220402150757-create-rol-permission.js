'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('RolPermission', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      RolId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'Rol', key: 'id'}
      },
      PermissionId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'Permission', key: 'id'}
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('RolPermission');
  }
};