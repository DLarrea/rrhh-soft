'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Rol extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.Permission, {
        through: models.RolPermission,
        as: 'permissions',
        foreignKey: 'RolId'
      });
      this.hasMany(models.User, {
        as: 'users',
        foreignKey: 'RolId'
      });
    }
  }
  Rol.init({
    name: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: true
    }
  }, {
    sequelize,
    modelName: 'Rol',
    tableName: 'Rol'
  });
  return Rol;
};