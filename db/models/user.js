'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Rol);
    }
  }
  User.init({
    username: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    email: {
      type: DataTypes.STRING(150)
    },
    authType: {
      type: DataTypes.ENUM('LDAP', 'APP'),
      defaultValue: 'APP'
    },
    RolId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { model: 'Rol', key: 'id' }
    },
    lastLogon: {
      allowNull: true,
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'User'
  });
  return User;
};