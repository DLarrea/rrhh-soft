'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Permission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.Rol, {
        through: models.RolPermission,
        as: 'rols',
        foreignKey: 'PermissionId'
      })
    }
  }
  Permission.init({
    name: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: true
    }
  }, {
    sequelize,
    modelName: 'Permission',
    tableName: 'Permission'
  });
  return Permission;
};