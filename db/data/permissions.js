/**
 *  MODELS
 *  Configuration
 *  Rol
 *  Permission
 *  RolPermission
 *  User
 *  Office
 *  Company
 *  Resignation
 *  Modality
 *  Level
 *  Domain
 *  Person
 *  PersonFile
 *  Contractual
 *  Structure
 *  Log
 */

const functionMapper = {
    'createConfiguration': 'Create_Configuration',
    'updateConfiguration': 'Update_Configuration',
    'viewConfiguration': 'View_Configuration',
    'deleteConfiguration': 'Delete_Configuration',
    'createRol': 'Create_Rol',
    'updateRol': 'Update_Rol',
    'viewRol': 'View_Rol',
    'deleteRol': 'Delete_Rol',
    'createRolPermission': 'Create_RolPermission',
    'deleteRolPermission': 'Delete_RolPermission',
}

const permissions = [
    {
        id: 1,
        name: 'View_Configuration',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 2,
        name: 'Create_Configuration',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 3,
        name: 'Update_Configuration',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 4,
        name: 'Delete_Configuration',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 5,
        name: 'View_Rol',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 6,
        name: 'Create_Rol',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 7,
        name: 'Update_Rol',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 8,
        name: 'Delete_Rol',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 9,
        name: 'Create_RolPermission',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 10,
        name: 'Delete_RolPermission',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 11,
        name: 'View_User',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 12,
        name: 'Create_User',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 13,
        name: 'Update_User',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 14,
        name: 'Delete_User',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 15,
        name: 'View_Office',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 16,
        name: 'Create_Office',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 17,
        name: 'Update_Office',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 18,
        name: 'Delete_Office',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 19,
        name: 'View_Company',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 20,
        name: 'Create_Company',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 21,
        name: 'Update_Company',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 22,
        name: 'Delete_Company',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 23,
        name: 'View_Resignation',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 24,
        name: 'Create_Resignation',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 25,
        name: 'Update_Resignation',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 26,
        name: 'Delete_Resignation',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 27,
        name: 'View_Modality',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 28,
        name: 'Create_Modality',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 29,
        name: 'Update_Modality',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 30,
        name: 'Delete_Modality',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 31,
        name: 'View_Level',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 32,
        name: 'Create_Level',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 33,
        name: 'Update_Level',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 34,
        name: 'Delete_Level',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 35,
        name: 'View_Domain',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 36,
        name: 'Create_Domain',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 37,
        name: 'Update_Domain',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 38,
        name: 'Delete_Domain',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 39,
        name: 'View_Person',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 40,
        name: 'Create_Person',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 41,
        name: 'Update_Person',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 42,
        name: 'Delete_Person',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 43,
        name: 'Create_PersonFile',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 44,
        name: 'Delete_PersonFile',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 45,
        name: 'View_Contractual',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 46,
        name: 'Create_Contractual',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 47,
        name: 'Update_Contractual',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 48,
        name: 'Delete_Contractual',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 49,
        name: 'View_Structure',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 50,
        name: 'Create_Structure',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 51,
        name: 'Update_Structure',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 52,
        name: 'Delete_Structure',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 53,
        name: 'Create_Log',
        createdAt: new Date(),
        updatedAt: new Date()
    },
];

module.exports = { permissions, functionMapper };