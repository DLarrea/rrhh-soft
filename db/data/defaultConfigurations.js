let { permissions } = require('./permissions');
let idCounter = 1;

let rols = [
    {
        id: 1,
        name: 'ADMIN',
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: 2,
        name: 'VISITOR',
        createdAt: new Date(),
        updatedAt: new Date()
    }
]

let adminRols = () => {
    let permissionsAdmin = [];
    for(let i of permissions){
        permissionsAdmin.push({
            id: idCounter,
            RolId: rols[0].id,
            PermissionId: i.id,
            createdAt: new Date(),
            updatedAt: new Date()
        });
        idCounter += 1;
    }
    return permissionsAdmin;
};


module.exports = {
    rols,
    adminRols
}